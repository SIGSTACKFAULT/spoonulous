from docutils import nodes
from docutils.parsers.rst import Directive
from docutils.parsers.rst.directives import flag
from sphinx.util import logging

logger = logging.getLogger(__name__)


class Component(Directive):
    option_spec = {
        "name": str,
        "qual": flag,
        "quant": flag,
        "tonnage": int,
    }

    def run(self):
        qualitative = "qual" in self.options.keys()
        quantitative = "quant" in self.options.keys()
        type_word = "/".join(
            filter(
                lambda x: x != None,
                [
                    "qualitative" if qualitative else None,
                    "quantitative" if quantitative else None,
                ],
            )
        ).title()


        p = nodes.list_item(classes=["component"])
        p.append(nodes.strong(text=f"{self.options['name'].upper()}", classes=["component-name"]))
        p.append(nodes.emphasis(text=f"{type_word} component", classes=["component-type"]))
        if tonnage := self.options.get("tonnage"):
            temp = nodes.inline(text=f"{tonnage} TONNAGE", classes=["component-tonnage"])
            temp["classes"].append("asdf")
            p.append(temp)
        return [p]


def setup(app):
    app.add_directive("component", Component)
    return {
        "version": "0.1",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
