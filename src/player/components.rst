Components
==========

Qualitative Components
----------------------

.. component::
    :qual:
    :name: Command Center
    :tonnage: 2

.. component::
    :qual:
    :name: Datalink
    :tonnage: 2

.. component::
    :qual:
    :name: Afterburner

.. component::
    :qual:
    :name: Structural Reinforcement
    :tonnage: 12

.. component::
    :qual:
    :name: Sloped Armor
    :tonnage: 6

Quantitative Components
-----------------------

.. component::
    :quant:
    :name: Datalink
    :tonnage: 2

.. component::
    :quant:
    :name: Launcher

.. component::
    :quant:
    :name: Magazine
    :tonnage: 1

.. component::
    :quant:
    :name: Beam

.. component::
    :quant:
    :name: Broadkill Pod
    :tonnage: 4


Fuel
----

.. attention:: TODO