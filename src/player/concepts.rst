Basic Concepts
==============


.. _Vector Counter:

Vector Movement
---------------

:ref:`Ships` and :ref:`Missiles` have a **Vector Counter** associated with them,
which represents their position after then next :ref:`Drift Phase`. Velocity persists from turn to turn.

Ships and Missiles may change their velocity with the :ref:`BURN` action, which happens during the :ref:`Ship Phase`


Relative Velocity
-----------------

.. attention:: TODO

Lines
-----

Drawing a physical line on a hexmap, while intuitive, is necessarily ambiguous by its very nature.
The line may not be perfectly straight, it may not center exactly on it's ends, and it may not be immediately obvious whether a hex “should” count as being on the line even if both of those are true.

If the players cannot agree, use `Bresenham's Line Algorithm <https://en.wikipedia.org/wiki/Bresenham's_line_algorithm>`_

.. warning::
    TODO: A more tabletop-friendly version of Bresenham's