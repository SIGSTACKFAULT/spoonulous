Sequence Of Play
================

In each phase, players secretly decide what they would like to do (typically by writing it down), then reveal simultaneously.
Players may discuss what they are doing, but need not tell the truth.

Ship Phase
----------

All ships secretly plan rotation and burns, then they resolve as planned.
They do not move until :ref:`DRIFT PHASE`

Launch Phase
------------

All SHIPS secretly plan MISSILE launches (see MISSILE RULES), then resolve as planned.

#TODO where do missiles spawn?

Missile Phase
-------------

All MISSILES (including ones launched in the immediately previous phase) secretly plan BURNS, then resolve.
They do not move until :ref:`Drift Phase`

Beam Phase
----------

All SHIPS secretly plan BEAM fire, then resolve.
When a beam destroys a missile, any remaining beams targeting that missile may be retargeted against another missile in the same FIRING ARC.


Impact Phase
------------

Any :ref:`Missiles` in the same hexes as their target :ref:`Ships` allocate :ref:`Hits`.
:ref:`Hits` are not actually applied until the next :ref:`Damage Phase`
See :ref:`Missile Rules`.

Damage Phase
------------

Any :ref:`HITS` allocated to :ref:`SHIPS` in preceding phases are applied


Drift Phase
-----------

Every object moves to its :ref:`Vector Counter`, then its Vector Counter moves by the same amount.
Thus, velocity persists from turn to turn.