.. Low Spoonulous: Tabletop Spaceship Command documentation master file, created by
   sphinx-quickstart on Sun Dec 31 18:04:33 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Low Spoonulous: Tabletop Spaceship Command
==========================================

filler text

.. toctree::
   :caption: Player Guide
   :maxdepth: 2
   :glob:

   player/concepts
   player/sequence_of_play
   player/*