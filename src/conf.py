import sys, os

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Low Spoonulous: Tabletop Spaceship Command'
copyright = '2023 GammaRailgun, SIGSTACKFAULT, and others.'
author = 'GammaRailgun, SIGSTACKFAULT'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

sys.path.append(os.path.abspath("./_ext"))
extensions = ['sphinx.ext.autosectionlabel', 'component']


templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# html_theme = 'alabaster'
html_title = project
html_permalinks_icon = '<span>#</span>'
html_theme = 'furo'
html_static_path = ['_static']

html_theme_options = { }
html_css_files = [
    'css/custom.css',
]